import React from "react";
import { useState } from "react";
import ProgressBar from 'react-bootstrap/ProgressBar'

export default class App extends React.Component {
    
    constructor(props) {
      super(props);
      this.state = {
        error: null,
        isLoaded: false,
        items: []
      };
    }
  
    componentDidMount() {
      fetch("https://jsonplaceholder.typicode.com/userss")
        .then(res =>{
          if(!res.ok){
throw Error('could not load data')
          }
        return res.json()})
        .then(
          (result) => {
            this.setState({
              isLoaded: true,
              items: result
            },9000);
          },
          
         
          (error) => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        )
        
    }
    
  
    render() {
      const { error, isLoaded, items } = this.state;
      if (error) {
        return <div style={{color:"red",fontSize:"30px"}}>Error: {error.message}</div>;
      } else if (!isLoaded) {
        return <div>{
          isLoaded ? (
          <ProgressBar animated now={45} />
          ):(
              <h1>Successs</h1>
          )}</div>;
      } else {
        return (
          <ul>
            {items.map(item => (
              <li key={item.id}>
                {item.name} {item.price}
              </li>
            ))}
          </ul>
        );
      }
    }
  }