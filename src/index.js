import React from 'react';
import ReactDOM from 'react-dom';
import reportWebVitals from './reportWebVitals';
import Header from './Web-Demo/Component/Header'
import Footer from './Web-Demo/Component/Footer'
ReactDOM.render(
  <React.StrictMode>
    <Header />
    <Footer/>
  </React.StrictMode>,
  
  document.getElementById('root')
);

reportWebVitals();
