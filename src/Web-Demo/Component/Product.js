import React, { useEffect, useState } from "react";
import Spinner from 'react-bootstrap/Spinner'
import Card from 'react-bootstrap/Card'
import Button from "react-bootstrap/Button";


const Products = () => {
  const [data, setData] = useState([]);
  const [filter, setFilter] = useState(data);
  const [loading, setLoading] = useState(false);
  let componentMounted = true;

  useEffect(() => {
    const getProducts = async () => {
      setLoading(true);
      const response = await fetch("https://fakestoreapi.com/products");
      if (componentMounted) {
        setData(await response.clone().json());
        setFilter(await response.json());
        setLoading(false);
        console.log(filter)
      }
      return () => {
        componentMounted = false;
      }
    }
    getProducts();
  }, []);
  const Loading = () => {
    return (
      <>
        <Spinner animation="border" className="spinner"/>
      </>
    )
  }
  const ShowProducts = () => {
    return(
      <>
      <h1>Product List</h1>
      {
      filter.map((product) => {
        return (
          <>
            <div className="col-md-4 col-6" >
            <Card className="text-center p-2 m-2" style={{ width: '18rem',height:'30rem'}} key={product.id}>
  <Card.Img variant="top" src={product.image} style={{height:'16rem' }}/>
  <Card.Body>
    <Card.Title>{product.title}</Card.Title>
    <Card.Text >${product.price}</Card.Text>
    <Button variant="primary">Add to cart</Button>
  </Card.Body>
</Card>
            </div>
          </>
        )
      })
    }
      </>

    );
  };
  return (
    <div>
      <div className="container">
        <div className="row">
          {
            loading ? <Loading />:<ShowProducts/>
          }

        </div>
      </div>
    </div>
  );
}
export default Products;