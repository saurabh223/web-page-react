import React from "react";
import { useNavigate } from "react-router-dom";
function Profile(props){
    const navigate=useNavigate();
    const handleLogout = () => {
        localStorage.clear();
        navigate('/login');
      }
    return(
        <div>
            <h1>welcome to your profile {props.email }!</h1>
            <input type="button" onClick={handleLogout} value="Logout" />
        </div>
    )
}
export default Profile;