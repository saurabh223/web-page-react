import React, { useState,useEffect } from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button';
import { useNavigate } from 'react-router-dom';
import './Web.css'

const Login = () =>{
    const [email,setEmail]=useState();
    const [password, setPassword]= useState();
    const navigate= useNavigate();
    useEffect(()=>{
        const auth =localStorage.getItem('user');
        if(auth)
        {
          navigate('/');
        }
      },[navigate]);
    const handleLogin = async () =>{
        console.warn("email,password",email,password)
        let result = await fetch('https://mamazone.herokuapp.com/api/login',{
            method:'POST',
            body:JSON.stringify({email,password}),
            headers:{
                'Content-Type':'application/json'
            }  
        });
        result = await result.json();
        console.warn(result)
        if(result.success){
localStorage.setItem("user",JSON.stringify(result));
navigate("/profile")
        }else{
            alert("invalid login credentials") 
        }
    }
    return(
        <div className="login-background">
      <Form className='m-3'>
        <h1>Login Here</h1>
      <Form.Group className="mb-3 mt-2">
      <Form.Control type="email" placeholder="Email" name="email" onChange={(e)=>setEmail(e.target.value)} />
                </Form.Group>
                <Form.Group className="mb-3 mt-2">
                <Form.Control type="password" name="password" placeholder="password" onChange={(e)=>setPassword(e.target.value)} />
                </Form.Group>
      <Button type="button"  onClick={handleLogin} >Login</Button>
      </Form>
    </div>
    )
}
export default Login;