import React from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Home from './Home';
import Container from "react-bootstrap/esm/Container";
import { BellFill, Cart, CurrencyDollar, Download, Person, QuestionCircle } from "react-bootstrap-icons/build";
import Card from "./Card";
import Login from "./Login";
import Profile from './Profile'
import Acart from './Acart'
import './Web.css'
import Contact from "./Contact";
import NavDropdown from 'react-bootstrap/NavDropdown'
import Men from "./Men";

function Header(){
  const auth = localStorage.getItem('user');
  const handleLogout = () => {
    localStorage.clear();
  }
    return(
        <Router>
        <div>
          <Navbar bg="light" expand="lg">
            <Container>
              <Navbar.Brand href="#home">
                <img
                  alt=""
                  src="https://cdn4.iconfinder.com/data/icons/logos-3/600/React.js_logo-512.png"
                  width="30"
                  height="30"
                  className="d-inline-block align-top" 
                />{' '}
                Brand Name
              </Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto text-center">
                  <Link to="/" className="menu"><Nav.Link href="#home" >Home</Nav.Link></Link>
                  <NavDropdown title="Category" id="basic-nav-dropdown" className="menu">
          <NavDropdown.Item href="#action/3.1">Men</NavDropdown.Item>
          <NavDropdown.Item href="#action/3.2">Women</NavDropdown.Item>
          <NavDropdown.Item href="#action/3.3">Kids</NavDropdown.Item>
          <NavDropdown.Item href="#action/3.4">Kitchen and home</NavDropdown.Item>
          <NavDropdown.Item href="#action/3.4">Beauty</NavDropdown.Item>
          <NavDropdown.Item href="#action/3.4">New Arrival</NavDropdown.Item>

        </NavDropdown>
                  <Link to="/contact" className="menu"><Nav.Link href="#contact">Contact Us</Nav.Link>
                  </Link>
                  <Link to="/Product" className="menu"><Nav.Link href="#products">Produts</Nav.Link>
                  </Link>
                  <NavDropdown title="Collection" id="basic-nav-dropdown" className="menu">
          <NavDropdown.Item href="#action/3.1">Summer</NavDropdown.Item>
          <NavDropdown.Item href="#action/3.2">Winter</NavDropdown.Item>
          <NavDropdown.Item href="#action/3.3"><Link  to="/Men" className="User" >mon</Link>Monsoon</NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="#action/3.4">Special Collection</NavDropdown.Item>
        </NavDropdown>
        <NavDropdown title="More" id="basic-nav-dropdown" className="menu">
          <NavDropdown.Item href="#action/3.1"><BellFill size='22' className="me-1"/>  Notification</NavDropdown.Item>
          <NavDropdown.Item href="#action/3.2"><QuestionCircle size='22' className="me-1"/>24 X 7 support  </NavDropdown.Item>
          <NavDropdown.Item href="#action/3.3"><Download size='22' className="me-1" />Download App  </NavDropdown.Item>
          <NavDropdown.Item href="#action/3.4"><CurrencyDollar size='22' className="me-1" />Explore Vip Zone</NavDropdown.Item>
        </NavDropdown>
                  <input type='text' placeholder="Search" className="search" ></input>
                  
                </Nav>
              </Navbar.Collapse>
              <Link to="/Cart" className="me-2" ><Cart size={26}  color="black"/></Link>
              {auth ?
              <Link  to="/login" onClick={handleLogout} className="User"><Person size={26} color="black"/>LogOut  </Link>:<>
              <Link to="/login" className="User"><Person size={26} color="black"/>Log in  </Link>
              </>
}
            </Container>
          </Navbar>
          <Routes>
            <Route exact path='/' element={< Home />}></Route>
            <Route exact path='/Product' element={<Card />}></Route>
            <Route exact path='/login' element={<Login />}></Route>
            <Route exact path='/Profile' element={<Profile />}></Route>
            <Route exact path='/Cart' element={<Acart />} />
            <Route exact path='/contact' element={<Contact />} />
            <Route exact path='/Men' element={<Men /> } />


          </Routes>
        </div>
      </Router>
    )
}
export default Header;